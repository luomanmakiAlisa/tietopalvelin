<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h3>Lisää tiedostoo</h3>

    <form action="upload.php" method="post" enctype="multipart/form-data" role="form">
        <div>
            <label for="tiedosto">Kuva:</label>
            <input type="file" name="tiedosto" id="tiedosto">
        </div>
        <div>
            <button>Lataa</button>
            <button type="button" onclick="window.location='index.php';">Peruuta</button>
        </div>
    </form>
</body>

</html>