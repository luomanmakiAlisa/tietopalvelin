<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h3>Tiedoston tallennus</h3>

    <?php
    if ($_FILES['tiedosto']['error'] == UPLOAD_ERR_OK) {
        if ($_FILES['tiedosto']['size'] > 0) {
            $tyyppi = $_FILES['tiedosto']['type'];
            if (strcmp($tyyppi, "application/pdf") == 0) {
                $file = basename($_FILES['tiedosto']['name']);
                $folder = 'uploads/';

                if (move_uploaded_file($_FILES['tiedosto']['tmp_name'], "$folder$file")) {
                    print "<p>Tiedosto on tallennettu palvelimelle.</p>";
                    print "<a href='index.php'>Selaa tiedostoja</a>";
                } else {
                    print "<p>Tiedoston tallennuksessa tapahtui virhe.</p>";
                }
            } else {
                print "<p>Voit ladata vain pdf-tiedostoja.</p>";
            }
        }
    }
    ?>
</body>

</html>